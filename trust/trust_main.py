#!usr/bin/python
# -*- coding: utf-8 -*-
"""
## Tcr&Bcr Repertoire Utilities for Solid Tumors (TRUST)
"""

try:
    import pysam
except:
    print '''Please install pysam first: https://code.google.com/p/pysam/'''
    sys.exit()
import time
import pandas as pd
from trust.extract_informative_readpairs import *
from trust.process_seq import *
from trust.find_overlap_seq import *
from trust.build_seq_overlap_matrix import *
from trust.find_disjoint_community import *
from trust.assemble_seqs import *
from trust.detect_cdr3 import *
from trust.single_end_mode import ProcessSingleEndReads
from trust.parse_trust_res import *

'''
Max read number in IMGT gene loci
'''
OVERSIZE_NUMBER = 1000000


curDir=os.path.dirname(os.path.realpath(__file__))+'/'



def trust_main(opt):

    bamdir=opt.Directory
    curDir=os.path.dirname(os.path.realpath(__file__))+'/'
    if len(bamdir)>0:
        files=os.listdir(bamdir)
        files0=[]
        for ff in files:
            if ff[-4:]!='.bam':
                continue
            if 'unmapped.bam' in ff:
                continue
            if 'Locs.bam' in ff:
                continue
            ff=bamdir+'/'+ff
            files0.append(ff)
        files=files0
    else:
        files=[]
    
    File=opt.file
    if len(File)>0:
        files=[File]

    FileList=opt.files
    if len(FileList)>0:
        files=[]
        fL=open(FileList)
        for ff in fL.readlines():
            if ff.strip()[-4:]!='.bam':
                continue
            files.append(ff.strip())
    
    if len(files)==0:
        print "Please provide input file or make sure your bam files are in the path you provided."
        return None
    #geneFile=opt.GeneLoci
    #if len(geneFile)==0:
    #   raise "No gene annotation file specified!"
    Cov=opt.Coverage
    Err=opt.Error
    thr_L=opt.Length
    IT=opt.InsertThreshold
    Bcell=opt.Bcell
    SE=opt.SingleEnd
    HC=opt.HeavyChain
    WD=opt.WD
    LF=opt.Locus
    UD=opt.UD
    ReferenceGenome=opt.Genome
    NUM_OF_THREADS=opt.NumberofCores
    light_chain=opt.light_chain
    extension=opt.Extension
    fasta=True

    if ReferenceGenome not in ['hg19', 'hg38', 'mm10']:
        print "Error: Please provide the correct reference genome!"
        return None

    if len(WD)==0:
        WD='./'
    
    out=''

    if Bcell:
        print "----- Run BCR analysis -----"
        if light_chain:
            print "----- Analyze light chain -----"
            out='-BCR-LightChain'
            LocusFile=curDir+'data/'+ReferenceGenome+'/GeneLocation/Bcell_l.bed'
            geneFile=curDir+'data/'+ReferenceGenome+'/GeneLocation/BCRall_l.bed'
        else:
            print "----- Analyze heavy chain -----"
            out='-BCR-HeavyChain'
            LocusFile=curDir+'data/'+ReferenceGenome+'/GeneLocation/Bcell.bed'
            geneFile=curDir+'data/'+ReferenceGenome+'/GeneLocation/BCRall.bed'
    else:
        print "----- Run TCR analysis -----"
        out='-TCR-ALL'
        LocusFile=curDir+'data/'+ReferenceGenome+'/GeneLocation/Tcell.bed'
        geneFile=curDir+'data/'+ReferenceGenome+'/GeneLocation/TCRall.bed'
    
    gL,gN=ParseGeneLoci(geneFile)
    
    for ff in files: 

        print ff

        fname=ff.split('/')[-1]
        
        if not LF:
            if SE:
                sr=1
            else:
                try:
                    sr=ScreenGenome(ff,LocusFile,IT,WD, Bcell)
                    print "ScreenGenome Finished! sr = ", sr
                except ValueError:
                    continue
        else:
            sr=0


        if not LF:
            ffm=WD+'/'+fname+'-Locs.bam'
            ffu=WD+'/'+fname+'-unmapped.bam'
        else:
            ffm=ff
            ffu=re.sub('-Locs.bam','',ff)+'-unmapped.bam'

        rD, pRD, gD, N_all, geneCoverage, nr=AllocateReadsIntoGenes(ffm,gL,gN)
            
        print "Total reads extracted %d" %(N_all)
        print "Informative read pairs kept %d" %(len(pRD))

        if Bcell and (len(pRD) > 60000 or nr > 110):
            global MAX_READS_NUMBER 
            MAX_READS_NUMBER = 500
            
        if Cov:
            print "Write IMGT gene coverage"
            gC=open(WD+'/'+fname+out+'-coverage.txt','w')
            for kk in geneCoverage:
                gC.write(kk+'\t'+str(len(geneCoverage[kk]))+'\n')
            gC.close()
            print "Done!"

        if sr==0:   ## Paired end mode checks out

            print "Paired-end mode"

            MasterContig={}
            UR_all=[]

            time1=time.time()
            
            for kk in gD:

                print "---Processing %s." %(kk)
                vv=gD[kk]
                print "Reads:", len(vv)

                OI,UR=GetReadsOverlapByGene(vv,nr,Err,thr_L)
                UR_all+=UR
                
                #time2=time.time()
                #print 'PE: GetReadsOverlapByGene ...time elapsed %f' %(time2-time1)
                #time1=time.time()

                DC,OD=FindDisjointCommunities(OI)
                Contigs=[]

                #time2=time.time()
                #print 'PE: FindDisjointCommunities ...time elapsed %f' %(time2-time1)
                #time1=time.time()

                for dc in DC:
                   ## order readComm reads by using their mapped pair 
                    rList=[]
                    for i in dc:
                        vv = pRD[i]
                        if vv[0]==1:
                            rList.append((i, vv[1][1].pos,vv[1][0]))
                        if vv[0]==2:
                            rList.append((i, vv[1][0].pos,vv[1][1]))
                    rList_sorted=sorted(rList,key=lambda x:x[1])
                    dc_sorted=[]
                    for i in rList_sorted:
                        dc_sorted.append(i[0])
                
                    ASs=AssembleCommReads(dc_sorted,OD,OI,pRD,rD,nr=nr,thr_overlap=thr_L)
                    #ASs=AssembleCommReads(dc,OD,OI,pRD,rD,nr=nr,thr_overlap=thr_L)
                    for uu in ASs:
                        nk=ASs[uu][2]
                        Contigs.append(([uu,nk],ASs[uu][0]+ASs[uu][1],ASs[uu][3]))

                MasterContig[kk]=Contigs
                print "Contigs:", len(Contigs)
                
                #time2=time.time()
                #print 'PE: AssembleCommReads ...time elapsed %f' %(time2-time1)
                #time1=time.time()

            time2=time.time()
            print 'PE: GetReadsOverlapByGene, FindDisjointCommunities, AssembleCommReads ...time elapsed %f' %(time2-time1)
            

            time1=time.time()
            
            ContigFinalList=MergeMasterContig(MasterContig,pRD,rD,thr_L,Err)
            
            time2=time.time()
            print 'PE: MergeMasterContig...time elapsed %f' %(time2-time1)
            print "Final contigs assembled %d" %(len(ContigFinalList))
            

            time1=time.time()
            
            annList=[]
            for cc in ContigFinalList:
                tmpSeq=cc[0]
                a = cc[2].split('_')
                b = [i[:3] for i in a]
                b = list(set(b))
                if len(b) == 1:
                    geneType=b[0][2]
                else:
                    geneType=''
                ann=AnnotateCDR3(tmpSeq,pRD,cc[3],light_chain=light_chain, geneType=geneType, error=Err,overlap_thr=thr_L,Bcell=Bcell, genome=ReferenceGenome)
                annList.append(ann)
            
            time2=time.time()
            print 'PE: AnnotateCDR3...time elapsed %f' %(time2-time1)
            

            print "Process unmapped read pairs"
            if not UD:
                try:
                    annListU,ContigFinalListU=ProcessSingleEndReads(ffu,LocusFile,HeavyChain=HC,light_chain=light_chain,err=Err,overlap_thr=thr_L,fasta=fasta,unmapped=True,Bcell=Bcell, genome=ReferenceGenome)
                except IOError:
                    print "No unmapped file found!"
                    annListU,ContigFinalListU=[],[]
                annList+=annListU
                ContigFinalList+=ContigFinalListU

        if sr==1:       
            print "Switching to single end mode"
            annList,ContigFinalList=ProcessSingleEndReads(ff,LocusFile,HeavyChain=HC,light_chain=light_chain,err=Err,overlap_thr=thr_L,fasta=fasta,Bcell=Bcell, genome=ReferenceGenome)
            print "Number of annList %d" %(len(annList))
        

        Res=open(WD+'/'+fname+out+'.fa','w')
        Res.write('''## Command: trust -f %s -d %s -F %s -c %s -e %d -l %d -a %s -s %s -H %s -I %d -o %s -B %s
## Information line contains the following fields:
# File name
# Normalized read count, or relative expression
# Contig sequence length
# Total TCR reads count
# TRUST annotated variable gene
# TRUST annotated joining gene (and constant gene in the case of B cell heavy chain)
# Aligner reported gene (PE mode only)
# CDR3 amino acid sequence
# -log(E value), QC measure for mapping CDR3 contig to IMGT reference
# CDR3 DNA sequence
''' %(File,bamdir,FileList,Cov,Err,thr_L,fasta,SE,HC,IT,WD,Bcell))

        for ii in xrange(0,len(annList)):
            ann=annList[ii]
            if len(ann)>0:
                cc=ContigFinalList[ii]
                if sr==1 or SE:
                    motifGene=''
                else:
                    motifGene=cc[2]

                tmpInfo='>'+fname+'+est_clonal_exp='+str(ann[4])+'+contig_reads_count='+str(ann[6])+'+seq_length='+str(len(cc[0]))+'+est_lib_size='+str(N_all)+'+'+ann[2]+'+'+ann[3]+'+'+motifGene+'+'+ann[0]+'+minus_log_Eval='+str(ann[5])+'+'+ann[1]+'\n'
                Res.write(tmpInfo)
                Res.write(cc[0]+'\n')

        Res.close()

        trust_res=parse_trust_res(WD+'/'+fname+out+'.fa', Bcell, light_chain)
        
        if trust_res is not None:

            formatResult=process_trust_res(trust_res,light_chain=light_chain,error=Err,overlap_thr=thr_L, Bcell=Bcell, genome=ReferenceGenome)
            formatResult.to_csv(WD+'/'+fname+out+'.txt', index=False, sep='\t')

            if not Bcell and extension:

                MotifDict, DNAFaDict, MotifDictB, DNAFaDictB, MotifDictL, DNAFaDictL=GetCDR3Patterns(ReferenceGenome)
                trust_res_extension=partial_cdr3_extension(WD+'/'+fname+out+'.txt', DNAFaDict)
                trust_res_extension.to_csv(WD+'/'+fname+out+'-extended.txt', index=False, sep='\t')
    
    if ffu is not None:
        os.remove(ffu)
    return None


def main():
    (opt,_)=CommandLineParser()
    trust_main(opt)


if __name__=='__main__':
    main()


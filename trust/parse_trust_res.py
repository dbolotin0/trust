import pandas as pd
import regex
try:
    from gmpy import popcount as PopC
    print "Successfully import gmpy"
except:
    PopC = lambda x:bin(x).count('1')
from trust.extract_informative_readpairs import *
from trust.process_seq import *
from trust.find_overlap_seq import *
from trust.build_seq_overlap_matrix import *
from trust.find_disjoint_community import *
from trust.assemble_seqs import *
from trust.detect_cdr3 import *
from trust.single_end_mode import ProcessSingleEndReads

def parse_trust_res(filename, Bcell=False, light_chain=False):

    if Bcell and not light_chain:
        lenthro=29
    else:
        lenthro=20
    try:
        trust=pd.read_table(filename, sep='+',header=None, skiprows=12, na_filter=False)
    except ValueError:
        return None

    if trust is not None:
        
        a=trust.loc[range(0, trust.shape[0], 2)]
        b=trust.loc[range(1, trust.shape[0], 2), 0]
        a.index=b.index=range(0,trust.shape[0]/2)
        trust=pd.concat([a,b], axis=1, ignore_index=True)
        trust.columns=['filename', 'est_clonal_exp','contig_reads_count', 'totaldna_len', 'est_lib_size', 
                                   'Vgene', 'Jgene', 'reportgene', 'cdr3aa', 'minus_log_Eval',
                                   'cdr3dna', 'totaldna']
        trust.filename = trust.filename.str.replace('>', '')
        trust.est_clonal_exp = trust.est_clonal_exp.str.replace('est_clonal_exp=', '')
        trust.est_lib_size = trust.est_lib_size.str.replace('est_lib_size=', '')
        trust.minus_log_Eval = trust.minus_log_Eval.str.replace('minus_log_Eval=', '')
        trust.totaldna_len = trust.totaldna_len.str.replace('seq_length=', '')
        trust.contig_reads_count = trust.contig_reads_count.str.replace('contig_reads_count=', '')
        Cgene=trust.Jgene.str.split('|').str[1]
        Cgene=Cgene.rename("Cgene")
        Cgene[Cgene.isnull()]=''
        trust=pd.concat([trust,Cgene], axis=1, ignore_index=False)
        trust.Jgene=trust.Jgene.str.split('|').str[0]
        trust=trust[trust.cdr3aa != '']
        trust=trust.assign(cdr3aa_len = map(len, trust.cdr3aa))
        trust=trust[trust.cdr3aa_len > 4]
        trust=trust[trust.cdr3aa_len < 35]
        index=trust[((trust.Vgene == '')|(trust.Jgene == ''))&(trust.cdr3aa_len > lenthro)&(trust.Vgene.str[0:3]!='TRD')&(trust.Jgene.str[0:3]!='TRD')].index
        #|(trust.cdr3aa_len < 6)
        trustNew=trust.drop(index)
        trustNew=trustNew.iloc[:, [0,4,1,5,6,12,7,8,10,9,11,2]]
        trustNew=trustNew.sort_values(by='est_clonal_exp', ascending=False)
        trustNew=trustNew.drop_duplicates()
        return trustNew

    else: 
        
        return None
        

def CompareSuffixByBitSeq(x,y,nx,ny,err=1):
    
    if x[0]==y[0] and x[1]==y[1]:
        return (nx,0)

    err_min=[999,999]
    suffL=[-1,-1]
    bitIdx=[-1,-1]
    nm=min(nx,ny)
    OBJmin=9999

    for kk in xrange(10, nm):
        tmp=[0,0]
        for ss in xrange(0,2):
            xx=x[ss]
            yy=y[ss]
            yyk=yy>>(ny-kk)
            xxk0=xx>>kk
            xxk=xx-(xxk0<<kk)
            tmp[ss]=xxk^yyk
        #print tmp
        tmp1=tmp[0]|tmp[1]
        ee=PopC(tmp1)
        if ee<=err:
            err_min=ee
            suffL=kk
            bitIdx=tmp1

    for i in xrange(0, ny-nm):
        tmp=[0,0]
        for ss in xrange(0,2):
            xx=x[ss]
            yy=y[ss]
            yyk0=yy-((yy>>(ny-i))<<(ny-i))
            yyk=yyk0>>(ny-nm-i)
            tmp[ss]=xx^yyk
        tmp1=tmp[0]|tmp[1]
        ee=PopC(tmp1)
        if ee<=err:
            err_min=ee
            suffL=nm
            bitIdx=tmp1
    if err_min <= err:
        return (suffL, err_min)
    else:
        return (0,0) 

def CompareSuffixSeq_Python(seq1,seq2,err=1):
    '''
    Compare a pair of sequences for their overlap in 4 scenarios: x,y; y,x; xc,y; y,xc
    '''
    seq1c=ReverseCompSeq(seq1)
    x=ConvertDNAtoBinary(seq1)
    y=ConvertDNAtoBinary(seq2)
    xc=ConvertDNAtoBinary(seq1c)
    n1=len(seq1)
    n2=len(seq2)
    suffList=[]
    suf=CompareSuffixByBitSeq(x,y,n1,n2,err=err)
    suffList.append(suf)
    suf=CompareSuffixByBitSeq(y,x,n2,n1,err=err)
    suffList.append(suf)
    suf=CompareSuffixByBitSeq(xc,y,n1,n2,err=err)
    suffList.append(suf)
    suf=CompareSuffixByBitSeq(y,xc,n2,n1,err=err)
    suffList.append(suf) 
    MaxL=-1
    MaxOs=-1
    MaxErr=0
    for i in xrange(0,4):
        tmp=suffList[i]
        if tmp[0]>MaxL:
            MaxL=tmp[0]
            MaxOs=i
            MaxErr=tmp[1]
        elif tmp[0]==MaxL and tmp[1]<MaxErr:
            MaxL=tmp[0]
            MaxOs=i
            MaxErr=tmp[1]

    return ((MaxL,MaxErr),MaxOs)       
    

def process_trust_res(trust_res, light_chain=False, error=1, overlap_thr=10, Bcell=False, genome='hg19'):

    trust_res_new=pd.DataFrame(columns=trust_res.columns)

    for i in trust_res.cdr3dna.unique():
        sameCDR3_res=trust_res[trust_res.cdr3dna==i]
        if len(sameCDR3_res) == 1:
            trust_res_new=trust_res_new.append(sameCDR3_res)
        elif len(sameCDR3_res) > 1:
            contigDic={}
            OverlapInfo={}
            for i in xrange(len(sameCDR3_res)):
                query=sameCDR3_res.totaldna.iloc[i]
                contigDic['N'+str(i)]=query
                for j in range(i+1,len(sameCDR3_res)):
                    ref=sameCDR3_res.totaldna.iloc[j]
                    #print query
                    #print ref
                    OP=CompareSuffixSeq_Python(query, ref, 10)
                    if OP[0][0] == min(len(query), len(ref)):
                        continue
                    else:
                        if OP[0][0]>=25:
                            kk='N'+str(i)+'\t'+'N'+str(j)
                            OverlapInfo[kk]=OP
            
            if len(OverlapInfo.keys()) == 0:
                a=sameCDR3_res.totaldna.str.len()
                maxlenCDR3_res=sameCDR3_res.loc[a.idxmax()]
                maxlenCDR3_res.est_clonal_exp=sum(map(float, sameCDR3_res.est_clonal_exp.tolist()))
                maxlenCDR3_res.contig_reads_count=sum(map(float, sameCDR3_res.contig_reads_count.tolist()))
                trust_res_new=trust_res_new.append(maxlenCDR3_res)
            else:
                #print finalSeq, len(finalSeq)
                #print OverlapInfo
                DC_c,OD_c=FindDisjointCommunities(OverlapInfo)
                ContigFinalList=[]
                for dc_c in DC_c:
                    if len(dc_c)>1: 
                        assContig=AssembleCommReads(dc_c,OD_c,OverlapInfo,None,None,contigDic,mode='contig',thr_overlap=25)
                        for kka in assContig:
                            
                            ggNs=[]
                            vva=assContig[kka]
                            vv=vva[0]
                            Nreads=0
                            est_clonal_exp=0
                            ReadsInfo=[]
                            for contig_ID in vv:
                                temp=int(contig_ID.replace('N', ''))
                                est_clonal_exp+=float(sameCDR3_res.est_clonal_exp.iloc[temp])
                                Nreads+=int(sameCDR3_res.contig_reads_count.iloc[temp])
                                ggNs.append(contig_ID)
                                ReadsInfo+=sameCDR3_res.reportgene.iloc[temp].split('_')

                            ReadsInfo=list(set(ReadsInfo))
                            
                            ann=AnnotateCDR3(kka, None, ReadsInfo, light_chain=light_chain,error=error,overlap_thr=overlap_thr,Bcell=Bcell, genome=genome)
                            
                            if len(ann)>0:
                                tmpres=sameCDR3_res.iloc[0]
                                tmpres.est_clonal_exp=est_clonal_exp
                                tmpres.contig_reads_count=Nreads
                                tmpres.Vgene=ann[2]
                                if ann[3] !='':
                                    tmpres.Jgene=ann[3].split('|')[0]
                                    tmpres.Cgene=ann[3].split('|')[1]
                                tmpres.totaldna=kka
                                trust_res_new=trust_res_new.append(tmpres)
                            #ContigFinalList.append((kka,Nreads,'__'.join(list(set(ggNs))),ReadsInfo))
                    else:
                        temp=int(dc_c[0].replace('N', ''))
                        trust_res_new=trust_res_new.append(sameCDR3_res.iloc[temp])
    #trustNew=trust_res_new.sort_values(by='est_clonal_exp', ascending=False)
    dupIndex=[]
    for i in xrange(trust_res_new.shape[0]):
        seq1=trust_res_new.cdr3dna.iloc[i]
        seq1=re.compile(seq1)
        match=filter(seq1.match, trust_res_new.cdr3dna.tolist())
        if len(match)>1:
            #print match
            dupIndex.append(i)
    #print dupIndex
    trust_res_new.drop(trust_res_new.index[dupIndex], inplace=True)
    return trust_res_new  


def trim_geneDNAseq(gene_seq, geneType, motif):
    
    AAList=[]
    for ff in [0,1,2]:
        AAseqs=BreakSeqIntoAAcodes(gene_seq,frame=ff)
        AA=''
        for i in AAseqs:
            if len(i)!=3:
                continue
            AA+=AAcode[i]
        match = regex.search(motif, AA)
        if match:
            if geneType=='v':
                gene_seq=gene_seq[3*match.start()+ff:]
            else:
                gene_seq=gene_seq[:3*(match.start()+1)+ff]
            return gene_seq
    return None

def cdr3_extension(partialCDR3dna, geneType, faDict):

    cdr3ComLen=9
    geneComLen=50

    partialCDR3dna
    MatchList=[]
    StartList=[]
    matchRes=[]
    maxErr=1

    if geneType=='v':

        ss=partialCDR3dna[0:cdr3ComLen]
        motif='C.SS'

        for gene in faDict:

            seq=faDict[gene].upper()[-geneComLen:]
            
            gene_seq=trim_geneDNAseq(seq, geneType, motif)

            if gene_seq is None:
                continue

            match = regex.search('('+ss+')' + '{s<='+str(maxErr)+'}', gene_seq)
            
            if match:
                matchRes.append(gene_seq)
                MatchList.append(gene)
                StartList.append(match.start())

    elif geneType=='j':

        ss=partialCDR3dna[-cdr3ComLen:]
        motif='[FW]G.G'

        for gene in faDict:

            seq=faDict[gene].upper()[0:geneComLen]
            
            gene_seq=trim_geneDNAseq(seq, geneType, motif)

            if gene_seq is None:
                continue

            match = regex.search('('+ss+')' + '{s<='+str(maxErr)+'}', gene_seq)
            
            if match:
                matchRes.append(gene_seq)
                MatchList.append(gene)
                StartList.append(match.start())
                

    else:
        print 'Unknow gene type'
        return None
        
    if len(MatchList) > 0:

        if geneType=='v':

            bestIdx=StartList.index(min(StartList))
            bestMatchGene=MatchList[bestIdx]
            newSeq=matchRes[bestIdx][:StartList[bestIdx]] + partialCDR3dna

        elif geneType=='j':

            bestIdx=StartList.index(max(StartList))
            bestMatchGene=MatchList[bestIdx]
            newSeq=partialCDR3dna+matchRes[bestIdx][StartList[bestIdx]+cdr3ComLen:]

        bestMatchGene='>'+bestMatchGene.split('|')[1]

        AAseqs=BreakSeqIntoAAcodes(newSeq,0)
        newSeqAA=''
        for i in AAseqs:
            if len(i)!=3:
                continue
            newSeqAA+=AAcode[i]
        #print  partialCDR3dna, newSeq, bestMatchGene   
        return (newSeq, bestMatchGene, newSeqAA)
    
    else:
        return None

def partial_cdr3_extension(filename, DNAFaDict):

    trust_res=pd.read_table(filename, na_filter=False)
    
    vPartialIndex=trust_res[trust_res.Vgene==''].index
    for i in vPartialIndex:
        if trust_res.loc[i, 'Jgene']=='':
            continue
        chain=trust_res.loc[i, 'Jgene'][2]+'V'
        faDict=DNAFaDict[chain]
        partialCDR3dna=trust_res.loc[i, 'cdr3dna']
        extension_res=cdr3_extension(partialCDR3dna, 'v', faDict)
        if extension_res is not None:
            trust_res.loc[i, 'cdr3dna']=extension_res[0]
            trust_res.loc[i, 'Vgene']=extension_res[1]
            trust_res.loc[i, 'cdr3aa']=extension_res[2]

    jPartialIndex=trust_res[trust_res.Jgene==''].index
    for i in jPartialIndex:
        if trust_res.loc[i, 'Vgene']=='':
            continue
        chain=trust_res.loc[i, 'Vgene'][2]+'J'
        faDict=DNAFaDict[chain]
        partialCDR3dna=trust_res.loc[i, 'cdr3dna']
        extension_res=cdr3_extension(partialCDR3dna, 'j', faDict)
        if extension_res is not None:
            trust_res.loc[i, 'cdr3dna']=extension_res[0]
            trust_res.loc[i, 'Jgene']=extension_res[1]
            trust_res.loc[i, 'cdr3aa']=extension_res[2]
        
    return trust_res
  
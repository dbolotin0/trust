'''
Assemble reads or contigs, calculate normalized read count, or relative expression.
'''

import re
from itertools import chain
from copy import deepcopy
from collections import defaultdict
from trust.process_seq import *
from trust.build_seq_overlap_matrix import *
from trust.find_disjoint_community import *


def MergeCommReads(assembleReads,assembleSeqs,OverlapInfo,nr=50,thr_overlap=10):
    assembleReadsNew=deepcopy(assembleReads)
    assembleSeqsNew=deepcopy(assembleSeqs)
    while True:
        m=len(assembleReadsNew)
        if m<=1:
            break
        flag=0
        for ii in xrange(0,m):
            flag_innerloop=0
            for jj in xrange(ii,m):
                if jj==ii:
                    continue
                ss1=assembleReadsNew[ii]
                ss2=assembleReadsNew[jj]
                sq1=assembleSeqsNew[ii]
                sq2=assembleSeqsNew[jj]
                op=list(set(ss1)&set(ss2))
                if len(op)==0:
                    continue
                ## find overlap, calculate the start positions for each read in each set
                rr=op[0]
                idx1=ss1.index(rr)
                idx2=ss2.index(rr)
                sign1=sq1[idx1][1]
                sign2=sq2[idx2][1]
                ss2o=ss2
                sq2o=sq2
                if sign1!=sign2:
                    ss2=ss2[::-1]
                    sq2_new=[]
                    for tmp in sq2:
                        sq2_new=[(ReverseCompSeq(tmp[0]),-tmp[1])]+sq2_new
                    sq2=sq2_new
                if len(op)>1:
                    ## find if the reads are forming a loop
                    flag_loop=0
                    for rr1 in op[1:]:
                        if (ss1.index(rr1)-idx1)*(ss2.index(rr1)-idx2)<0:
                            ## find a loop, no merging
                            flag_loop=1
                            break
                    if flag_loop==1:
                        continue
                n1=len(ss1)
                n2=len(ss2)
                idx1=ss1.index(rr)
                idx2=ss2.index(rr)                   
                ss1_pos=[0]
                ss2_pos=[0]
                for kk in xrange(idx1,n1):
                    if kk==idx1:
                        continue
                    tmpKey=ss1[kk-1]+'\t'+ss1[kk]
                    if tmpKey not in OverlapInfo:
                        tmpKey=ss1[kk]+'\t'+ss1[kk-1]
                    tmpV=OverlapInfo[tmpKey]
                    ss1_pos+=[ss1_pos[-1]+nr-tmpV[0][0]]
                for kk in xrange(idx1,-1,-1):
                    if kk==idx1:
                        continue
                    tmpKey=ss1[kk+1]+'\t'+ss1[kk]
                    if tmpKey not in OverlapInfo:
                        tmpKey=ss1[kk]+'\t'+ss1[kk+1]
                    tmpV=OverlapInfo[tmpKey]
                    ss1_pos=[ss1_pos[0]-nr+tmpV[0][0]]+ss1_pos
                for kk in xrange(idx2,n2):
                    if kk==idx2:
                        continue
                    tmpKey=ss2[kk-1]+'\t'+ss2[kk]
                    if tmpKey not in OverlapInfo:
                        tmpKey=ss2[kk]+'\t'+ss2[kk-1]
                    tmpV=OverlapInfo[tmpKey]
                    ss2_pos+=[ss2_pos[-1]+nr-tmpV[0][0]]
                for kk in xrange(idx2,-1,-1):
                    if kk==idx2:
                        continue
                    tmpKey=ss2[kk+1]+'\t'+ss2[kk]
                    if tmpKey not in OverlapInfo:
                        tmpKey=ss2[kk]+'\t'+ss2[kk+1]
                    tmpV=OverlapInfo[tmpKey]
                    ss2_pos=[ss2_pos[0]-nr+tmpV[0][0]]+ss2_pos
                ## try to merge two sets
                flag_NoOverlap=0
                i=idx1
                j=idx2
                while i<n1 and j<n2:
                    if ss1[i]==ss2[j]:
                        i+=1
                        j+=1
                        continue
                    if ss1_pos[i]<=ss2_pos[j]:
                        tmpKey=ss1[i]+'\t'+ss2[j]
                        if tmpKey not in OverlapInfo:
                            tmpKey=ss2[j]+'\t'+ss1[i]
                        if tmpKey not in OverlapInfo:
                            flag_NoOverlap=1
                            break
                        i+=1
                    else:
                        tmpKey=ss1[i]+'\t'+ss2[j]
                        if tmpKey not in OverlapInfo:
                            tmpKey=ss2[j]+'\t'+ss1[i]
                        if tmpKey not in OverlapInfo:
                            flag_NoOverlap=1
                            break
                        j+=1
                i=idx1
                j=idx2
                while i>=0 and j>=0:
                    if ss1[i]==ss2[j]:
                        i-=1
                        j-=1
                        continue
                    if ss1_pos[i]<=ss2_pos[j]:
                        tmpKey=ss1[i]+'\t'+ss2[j]
                        if tmpKey not in OverlapInfo:
                            tmpKey=ss2[j]+'\t'+ss1[i]
                        if tmpKey not in OverlapInfo:
                            flag_NoOverlap=1
                            break
                        j-=1
                    else:
                        tmpKey=ss1[i]+'\t'+ss2[j]
                        if tmpKey not in OverlapInfo:
                            tmpKey=ss2[j]+'\t'+ss1[i]
                        if tmpKey not in OverlapInfo:
                            flag_NoOverlap=1
                            break
                        i-=1                                        
                if flag_NoOverlap==0:   ## Merging happens
                    flag=1
                    #print ii,jj,'merging'
                    newComm=[ss1[idx1]]
                    newSeqs=[sq1[idx1]]
                    i=idx1
                    j=idx2
                    while i<n1 and j<n2:
                        if ss1_pos[i]<=ss2_pos[j]:
                            if ss1[i] not in newComm:
                                newComm+=[ss1[i]]
                                newSeqs+=[sq1[i]]
                            else:
                                i+=1
                                if i>=n1:
                                    for j0 in xrange(j,n2):
                                        if ss2[j0] not in newComm:
                                            newComm+=[ss2[j0]]
                                            newSeqs+=[sq2[j0]]
                                    j=n2
                        else:
                            if ss2[j] not in newComm:
                                newComm+=[ss2[j]]
                                newSeqs+=[sq2[j]]
                            else:
                                j+=1
                                if j>=n2:
                                    for i0 in xrange(i,n1):
                                        if ss1[i0] not in newComm:
                                            newComm+=[ss1[i0]]
                                            newSeqs+=[sq1[i0]]
                                    i=n1
                    i=idx1
                    j=idx2
                    while i>=0 and j>=0:
                        if ss1_pos[i]<=ss2_pos[j]:
                            if ss2[j] not in newComm:
                                newComm=[ss2[j]]+newComm
                                newSeqs=[sq2[j]]+newSeqs
                            else:
                                j-=1
                                if j==-1:
                                    for i0 in xrange(i,-1,-1):
                                        if ss1[i0] not in newComm:
                                            newComm=[ss1[i0]]+newComm
                                            newSeqs=[sq1[i0]]+newSeqs
                                    i=-1
                        else:
                            if ss1[i] not in newComm:
                                newComm=[ss1[i]]+newComm
                                newSeqs=[sq1[i]]+newSeqs
                            else:
                                i-=1
                                if i==-1:
                                    for j0 in xrange(j,-1,-1):
                                        if ss2[j0] not in newComm:
                                            newComm=[ss2[j0]]+newComm
                                            newSeqs=[sq2[j0]]+newSeqs
                                    j=-1
                    assembleReadsNew.remove(ss1)
                    assembleReadsNew.remove(ss2o)
                    assembleReadsNew.append(newComm)
                    assembleSeqsNew.remove(sq1)
                    assembleSeqsNew.remove(sq2o)
                    assembleSeqsNew.append(newSeqs)
                    flag_innerloop=1
                    break
            if flag_innerloop==1:
                break
        if flag==0:
            break
    return assembleReadsNew,assembleSeqsNew                           


def EMcount(contigReads,countDict,max_iter=1000,thr_s=0.001):
    
    nc=len(contigReads)
    uniqueReads=[]
    sharedReads=[]
    
    for rr in countDict:
        if countDict[rr]>1:
            sharedReads.append(rr)
        else:
            uniqueReads.append(rr)
    
    uniqueCounts=[]
    sharedCounts=[]
    
    for cc in contigReads:
        temp=list(set(uniqueReads) & set(cc))
        uniqueCounts.append(len(temp))
        temp=list(set(sharedReads) & set(cc))
        sharedCounts.append(temp)
    
    rsDict={}
    
    for read in sharedReads:
        for ii in xrange(0,nc):
            if read in contigReads[ii]:
                if read not in rsDict:
                    rsDict[read]=[ii]
                else:
                    rsDict[read].append(ii)
    
    contigReadCount0=[]
    
    for cc in contigReads:
        contigReadCount0.append(len(cc))
    
    ii=0
    flag=0
    
    while ii<max_iter:
        contigReadCount=[]
        for jj in xrange(0,nc):
            nu=uniqueCounts[jj]
            ss=sharedCounts[jj]
            ns=0
            for rs in ss:
                shared_contig=rsDict[rs]
                Ns=0
                for sc in shared_contig:
                    Ns+=contigReadCount0[sc]
                ns+=float(contigReadCount0[jj])/Ns
            contigReadCount.append(nu+ns)
        max_diff=-1
        for jj in xrange(0,nc):
            diff=abs(contigReadCount0[jj]-contigReadCount[jj])
            if diff > max_diff:
                max_diff=diff
        if max_diff<=thr_s:
            flag=1
            break
        contigReadCount0=contigReadCount
        ii+=1
    
    if flag==0:
        print "No convergence"
    

    return contigReadCount


def AssembleCommReads(readComm,OpDict,OverlapInfo,PairedReadDict,readDict,contigDict={},nr=50,thr_overlap=10,mode="read"):
    
    if mode not in ['read','contig']:
        print 'Wrong mode input!'
        raise
    kks=OverlapInfo.keys()
    vDict={}

    for qn in list(readComm):
        vDict[qn]=1

    assembleReads=[]
    assembleSeqs=[]
    
    while vDict.values().count(1)>0:
        
        for qn in vDict.keys():
            if vDict[qn]==1:
                break

        vDict[qn]=0

        if mode=='read':
            tmp=PairedReadDict[qn]
            if tmp[0]==1:
                tmpSeq=tmp[1][0].seq
            if tmp[0]==2:
                tmpSeq=tmp[1][1].seq
            if tmp[0]==3:
                vDict[qn]=0
                continue
            #print tmpSeq, len(tmpSeq), nr
            if len(tmpSeq) is not nr:
                continue
            tmpSeq0=ConvertBitToDNA(ReverseComp(ConvertDNAtoBinary(tmpSeq),n=nr),n=nr)
        
        if mode=='contig':
            tmpSeq=contigDict[qn]
            tmpSeq0=ReverseCompSeq(tmpSeq)
        
        if len(assembleReads)==0:
            assembleReads.append([qn])
            assembleSeqs.append([(tmpSeq,1)])  
        else:
            flag=0
            na=len(assembleReads)
            
            for aa in xrange(0,na):
                tmpContig=assembleReads[aa]
                tmpSeqs=assembleSeqs[aa]
                ns=len(tmpContig)
                ns0=ns
                
                for ii in xrange(0,ns):
                    pp=tmpContig[ii]
                    pp_seq=tmpSeqs[ii]
                    
                    if pp in OpDict[qn]:
                        flag=1
                        tmp=pp+'\t'+qn
                        direct=0
                        
                        if tmp in OverlapInfo:
                            vv=OverlapInfo[tmp]
                            direct=1
                        else:
                            vv=OverlapInfo[qn+'\t'+pp]
                        
                        if direct==0:                   
                            if vv[1]==0:
                                if pp_seq[1]==1:
                                    if ii==0:
                                        tmpContig=[qn]+tmpContig
                                        ns0=ns+1
                                        tmpSeqs=[(tmpSeq,1)]+tmpSeqs
                                if pp_seq[1]==-1:
                                    if ii==ns0-1:
                                        tmpContig+=[qn]
                                        tmpSeqs+=[(tmpSeq0,-1)]
                            
                            if vv[1]==1:
                                if pp_seq[1]==1:
                                    if ii==ns0-1:
                                        tmpContig+=[qn]
                                        tmpSeqs+=[(tmpSeq,1)]
                                if pp_seq[1]==-1:
                                    if ii==0:
                                        ns0=ns+1
                                        tmpContig=[qn]+tmpContig
                                        tmpSeqs=[(tmpSeq0,-1)]+tmpSeqs
                            
                            if vv[1]==2:
                                if pp_seq[1]==1:
                                    if ii==0:
                                        ns0=ns+1
                                        tmpContig=[qn]+tmpContig
                                        tmpSeqs=[(tmpSeq0,-1)]+tmpSeqs
                                if pp_seq[1]==-1:
                                    if ii==ns0-1:
                                        tmpContig+=[qn]
                                        tmpSeqs+=[(tmpSeq,1)]
                            
                            if vv[1]==3:
                                if pp_seq[1]==1:
                                    if ii==ns0-1:
                                        tmpContig+=[qn]
                                        tmpSeqs+=[(tmpSeq0,-1)]
                                if pp_seq[1]==-1:
                                    if ii==0:
                                        ns0=ns+1
                                        tmpContig=[qn]+tmpContig
                                        tmpSeqs=[(tmpSeq,1)]+tmpSeqs
                        
                        if direct==1:                                                                    
                                if vv[1]==1:
                                    if pp_seq[1]==1:
                                        if ii==0:
                                            tmpContig=[qn]+tmpContig
                                            ns0=ns+1
                                            tmpSeqs=[(tmpSeq,1)]+tmpSeqs
                                    if pp_seq[1]==-1:
                                        if ii==ns0-1:
                                            tmpContig+=[qn]
                                            tmpSeqs+=[(tmpSeq0,-1)]
                                
                                if vv[1]==0:
                                    if pp_seq[1]==1:
                                        if ii==ns0-1:
                                            tmpContig+=[qn]
                                            tmpSeqs+=[(tmpSeq,1)]
                                    if pp_seq[1]==-1:
                                        if ii==0:
                                            ns0=ns+1
                                            tmpContig=[qn]+tmpContig
                                            tmpSeqs=[(tmpSeq0,-1)]+tmpSeqs
                                
                                if vv[1]==3:
                                    if pp_seq[1]==-1:
                                        if ii==0:
                                            ns0=ns+1
                                            tmpContig=[qn]+tmpContig
                                            tmpSeqs=[(tmpSeq,1)]+tmpSeqs
                                    if pp_seq[1]==1:
                                        if ii==ns0-1:
                                            tmpContig+=[qn]
                                            tmpSeqs+=[(tmpSeq0,-1)]
                                
                                if vv[1]==2:
                                    if pp_seq[1]==-1:
                                        if ii==ns0-1:
                                            tmpContig+=[qn]
                                            tmpSeqs+=[(tmpSeq,1)]
                                    if pp_seq[1]==1:
                                        if ii==0:
                                            ns0=ns+1
                                            tmpContig=[qn]+tmpContig
                                            tmpSeqs=[(tmpSeq0,-1)]+tmpSeqs
                
                assembleReads[aa]=tmpContig
                assembleSeqs[aa]=tmpSeqs
            
            if flag==0:
                    assembleReads.append([qn])
                    assembleSeqs.append([(tmpSeq,1)])
    try:
        assembleReadsNew,assembleSeqsNew=MergeCommReads(assembleReads,assembleSeqs,OverlapInfo,nr=nr,thr_overlap=thr_overlap)
    except KeyError:
        print "Warning: recurring reads in different positions. Increase overlap threshold length (-l) to reduce randomness."
        assembleReadsNew=assembleReads
        assembleSeqsNew=assembleSeqs
    #print '## Add remaining reads here'
    remainReads=[[] for x in xrange(len(assembleReadsNew))]
    for rr in readComm:
        for ii in xrange(0,len(assembleReadsNew)):
            readSet=assembleReadsNew[ii]
            if rr in readSet:
                continue
            else:
                for read in readSet:
                    tmpKey=rr+'\t'+read
                    if tmpKey not in OverlapInfo:
                        tmpKey=read+'\t'+rr
                    if tmpKey not in OverlapInfo:
                        continue
                    else:
                        if rr not in remainReads[ii]:
                            remainReads[ii].append(rr)
    #print '## Count reads in each contig'                        
    temp=list(chain(*assembleReadsNew))+list(chain(*remainReads))
    countDict=defaultdict(int)
    for ww in temp:
        countDict[ww]+=1
    contigReads=[]
    contigReadCount=[]
    for ii in xrange(len(assembleReadsNew)):
        temp=assembleReadsNew[ii]+remainReads[ii]
        temp=list(set(temp))
        contigReads.append(temp)
        contigReadCount.append(len(temp))
    #contigReadCount=EMcount(contigReads,countDict)
    #print '## stitch up different small contigs'
    AssSeqs={}
    for cc in xrange(0,len(assembleReadsNew)):
        contig=assembleReadsNew[cc]
        Seqs=assembleSeqsNew[cc]
        rR=remainReads[cc]
        rC=contigReadCount[cc]
        seq=''
        nn=len(contig)
        if nn+len(rR)<1:
            continue
        i=0
        rr_contig=[]
        rR_info=[]
        for rr in rR:
            if mode=='read':
                tmp=PairedReadDict[rr]      ## major change. was: tmp=readDict[rr]
                if tmp[0]==1:   ## was tmp[0].rname==-1
                    rR_info.append(tmp[1][0])
                elif tmp[0]==2:
                    rR_info.append(tmp[1][1])
            if mode=='contig':
                rR_info.append(rr)
        while i<nn:
            p1=contig[i]
            tag=Seqs[i][1]
            
            if i==0:
                
                if mode=='read':
                    tmp=PairedReadDict[p1]
                    if tmp[0]==1:
                        if tag==1:
                            seq=tmp[1][0].seq
                        else:
                            seq=ConvertBitToDNA(ReverseComp(ConvertDNAtoBinary(tmp[1][0].seq),n=nr),n=nr)
                            rr_contig.append(tmp[1][0])
                    else:
                        if tag==1:
                            seq=tmp[1][1].seq
                        else:
                            seq=ConvertBitToDNA(ReverseComp(ConvertDNAtoBinary(tmp[1][1].seq),n=nr),n=nr)
                            rr_contig.append(tmp[1][1])
                
                if mode=='contig':
                    if tag==1:
                        seq=contigDict[p1]
                    else:
                        seq=ReverseCompSeq(contigDict[p1])
                    rr_contig.append(p1)
                
                i+=1
                continue
            
            p0=contig[i-1]
            tmp=p0+'\t'+p1
            
            if tmp in OverlapInfo:
                vv=OverlapInfo[tmp]
            else:
                tmpKey=p1+'\t'+p0
                if tmpKey not in OverlapInfo:
                    ## same read assembled in different contigs, rare and potentially erroneous
                    break
                vv=OverlapInfo[p1+'\t'+p0]
            
            if mode=='read':
                tmp=PairedReadDict[p1]
                if tmp[0]==1:
                    rr_contig.append(tmp[1][0])
                elif tmp[0]==2:
                    rr_contig.append(tmp[1][1])
            
            if mode=='contig':
                rr_contig.append(p1)
            
            seq+=Seqs[i][0][vv[0][0]:]                      
            i+=1
        AssSeqs[seq]=(rr_contig,rR_info,rC,contig)
    

    return AssSeqs


def MergeUnmappedPairs(Contigs):
    readNames=[]
    NewContigs=[]
    for cc in Contigs:
        if len(cc[-1])>1:
            NewContigs.append(cc)
            continue
        qname=cc[-1][0]
        if re.search('[./][12]$', qname):
            qname=qname[0:-2]
        if qname not in readNames:
            NewContigs.append(cc)
            readNames.append(qname)
    return NewContigs
    

def MergeMasterContig(MasterContig,pairedReadDict,readDict,overlap_thr=10,error=1):

    ContigDict={}

    for kk in MasterContig:
        vv=MasterContig[kk]
        for cc in xrange(0,len(vv)):
            nn=kk+'_'+str(cc)
            ContigDict[nn]=vv[cc][0][0]

    OI_c=GetSeqOverlap(ContigDict.values(),ContigDict.keys(),2*overlap_thr,err=error)
    DC_c,OD_c=FindDisjointCommunities(OI_c)
    ContigFinalList=[]
    
    for dc_c in DC_c:
        if len(dc_c)>1: 
            assContig=AssembleCommReads(dc_c,OD_c,OI_c,pairedReadDict,readDict,ContigDict,mode='contig',thr_overlap=int(1.5*overlap_thr))
            for kka in assContig:
                ggNs=[]
                vva=assContig[kka]
                vv=vva[0]+vva[1]
                Nreads=0
                ReadsInfo=[]
                for contig_ID in vv:
                    temp=contig_ID.split('_')
                    ggNs.append(temp[0])
                    Nreads+=MasterContig[temp[0]][int(temp[1])][0][1]
                    ReadsInfo+=MasterContig[temp[0]][int(temp[1])][2]
                ContigFinalList.append((kka,Nreads,'_'.join(list(set(ggNs))),ReadsInfo))
        else:
            temp=dc_c[0].split('_')
            vv=MasterContig[temp[0]][int(temp[1])]
            ContigFinalList.append((vv[0][0],vv[0][1],temp[0],vv[2]))                                                
    

    return ContigFinalList

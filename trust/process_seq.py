'''
Encoding and decoding dna sequence，reverse sequence
'''

BinCodeDict={'A':(0,0),'T':(1,1),'C':(1,0),'G':(0,1)}
DNACodeDict={'00':'A','11':'T','10':'C','01':'G'}
RCdict={'A':'T','T':'A','C':'G','G':'C','N':'N'}

def ReverseCompSeq(seq):
    seq_rc=''
    seqL=list(seq)
    for ss in seqL:
        seq_rc=RCdict[ss]+seq_rc
    return seq_rc


def ReverseComp(x,n=50):
    x0=2**n-1-x[0]
    x1=2**n-1-x[1]
    s0=bin(x0)[2:].zfill(n)
    x0c=int(s0[::-1],2)
    s1=bin(x1)[2:].zfill(n)
    x1c=int(s1[::-1],2)
    return (x0c,x1c)


def ConvertDNAtoBinary(seq):
    '''
    Coding DNA sequence as binary integer
    '''
    binArray=[]
    for s in list(seq):
        #if s=='N':
        #   s=BinCodeDict.keys()[random.randint(0,3)]
        if(s == '.'):
            continue
        binArray.append(BinCodeDict[s])
    a1=''
    b1=''
    for k in binArray:
        a1+=str(k[0])
        b1+=str(k[1])
    return (int(a1,2),int(b1,2))


def ConvertBitToDNA(x,n=50):
    a0=format(x[0],'0'+str(n)+'b')
    a1=format(x[1],'0'+str(n)+'b')
    seq=''
    for i in xrange(0,n):
        seq+=DNACodeDict[a0[i]+a1[i]]
    return seq

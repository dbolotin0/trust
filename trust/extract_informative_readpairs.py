'''
Align paired-end reads to human genome and extract informative read pairs in the TCR region
'''


import re
import numpy as np
try:
    import pysam
except:
    print '''Please install pysam first: https://code.google.com/p/pysam/'''
    sys.exit()

'''
MAPQ Cutoff
'''
MAPQ_CUTOFF = 0.9


'''
Align paired-end reads to human genome and extract informative read pairs in the TCR region
'''

def ParseGeneLoci(geneFile,header=False):
    ## input is bed file with gene name as the last column
    gg=open(geneFile)
    if header:
        gg.readline()   ## get rid of header line
    geneLoci=[]
    geneName=[]
    for line in gg.readlines():
        ww=line.strip().split()
        LL=ww[0]+':'+ww[1]+'-'+ww[2]
        gene=ww[3]
        geneLoci.append(LL)
        geneName.append(gene)
    return geneLoci,geneName


def ScreenGenome(fname,LocusFile,InsThr=10,saveDir='./', Bcell=False):

    print "Screen for informative reads"
    
    gLocus=open(LocusFile)
    Locs=[]
    for line in gLocus.readlines():
        Locs.append(line.strip().split('\t'))
        
    seqDir={}
    CHRlist=[]
    stlist=[]
    edlist=[]
    nr=0
    SEcheck=0
    
    for Loc in Locs:

        print Loc
        
        CHR=Loc[0]
        st=int(Loc[1])
        ed=int(Loc[2])
        CHRlist.append(CHR)
        stlist.append(st)
        edlist.append(ed)
        handle=pysam.Samfile(fname)
        
        if 'chr7' in handle.references:
            if 'chr' not in CHR:
                CHR='chr'+CHR
        else:
            CHR=re.sub('chr','',CHR)

        for read in handle.fetch(CHR,st,ed):
            if nr==0:
                nr=read.rlen
            #if read.mapq<=30:
            #   continue
            if read.qname not in seqDir:
                seqDir[read.qname]=[read]
            else:
                seqDir[read.qname].append(read)
    
    print '''Pair reads in the region'''
    count=1
    count_r=0
    InsertSize=[]
    refs=handle.references
    unmapped_reads=[]
    paired_flag=0

    for rr in handle.fetch(until_eof=True):
        
        if rr.flag & 1 == 1 and paired_flag == 0:
            paired_flag=1
        
        #if rr.flag & 4 > 0:
            #unmapped_reads.append(rr)
        
        if count % 1000000 == 0:
            print "--processed %d reads" %(count)
            if paired_flag==0:
                SEcheck=1
                #return 1
        
        count+=1
        
        if count %10000==0 and SEcheck==0:
            insize=np.fabs(rr.pnext-rr.pos)+rr.rlen
            if insize<1000:
                InsertSize.append(insize)
            if len(InsertSize)>=1000:
                medIns=np.median(np.array(InsertSize))
                if medIns<= 2*nr - InsThr: 
                    SEcheck=1     
                    ## This is the case when insert size is too small to apply paired-end algorithm. Switch to single end mode automatically.
                    print "Insert size is too small to apply paired-end algorithm. Switch to single end mode automatically"
                    #return 1
        
        if rr.qname in seqDir:
            vv=seqDir[rr.qname]
            flag=0
            for v in vv:
                if rr.seq==v.seq:
                    flag=1
            if flag==0:
                seqDir[rr.qname].append(rr)
        else:
            if re.search('[/][12]$', rr.qname):
                if re.search('[/]1$', rr.qname):
                    qname_paired=re.sub('[/]1$','/2',rr.qname)
                else:
                    qname_paired=re.sub('[/]2$','/1',rr.qname)
            elif re.search('[.][12]$', rr.qname):
                if re.search('[.]1$', rr.qname):
                    qname_paired=re.sub('[.]1$','.2',rr.qname)
                else:
                    qname_paired=re.sub('[.]2$','.1',rr.qname)
            else:
                qname_paired=rr.qname
            
            if qname_paired in seqDir:      
                seqDir[qname_paired].append(rr)
                count_r+=1
                #if count_r % 100 ==0:
                    #print refs[rr.rname], rr.pos
                    #print "---retrived %d unmapped reads" %(count_r)

            elif rr.flag & 4 > 0 and len(set(rr.seq))>2:
                   unmapped_reads.append(rr)
        
    
    if paired_flag==0:  ## In case the total library has fewer than 1M reads
        print("The total library has fewer than 1M reads!")
        SEcheck=1
        #return 1
    
    nfname=saveDir+fname.split('/')[-1]+'-Locs.bam'
    HH=handle.header
    ghandle=pysam.Samfile(nfname,mode='wb',header=HH,referencenames=handle.references,referencelengths=handle.nreferences)
    
    for kk in seqDir:
        for read in seqDir[kk]:
            ghandle.write(read)
    
    pDict={}

    for rr in unmapped_reads:
        if rr.qname not in pDict:
            newName=rr.qname
            if re.search('[/][12]$', rr.qname):
                if re.search('[/]1$', rr.qname):
                    newName=re.sub('[/]1$','/2',rr.qname)
                else:
                    newName=re.sub('[/]2$','/1',rr.qname)
            elif re.search('[.][12]$', rr.qname):
                if re.search('[.]1$', rr.qname):
                    newName=re.sub('[.]1$','.2',rr.qname)
                else:
                    newName=re.sub('[.]2$','.1',rr.qname)
            if newName in pDict:
                pDict[newName].append(rr)
            else:
                pDict[rr.qname]=[rr]
        else:
             pDict[rr.qname].append(rr)

    paired_unmapped=[]

    if nr > 90 and Bcell:
        print ('Read length:', nr)
        for kk in pDict:
            if len(pDict[kk])==2:
                paired_unmapped.append(pDict[kk])
    else:
        print ('Read length:', nr)
        for kk in pDict:
            if len(pDict[kk])>=1:
                paired_unmapped.append(pDict[kk])

    unmapped_file_name=fname.split('/')[-1]+'-unmapped.bam'
    uhandle=pysam.Samfile(saveDir+unmapped_file_name,mode='wb',header=HH,referencenames=handle.references,referencelengths=handle.nreferences)
    
    for kk in paired_unmapped:
        for read in kk:
            uhandle.write(read)
    
    return SEcheck

def my_mapping_quality_score(cigartuples, readLength):
    machedLength=0
    for i in cigartuples:
        if i[0] == 0: #matched position
            machedLength+=i[1]
    return machedLength/float(readLength)


def AllocateReadsIntoGenes(ffm,geneLoci,geneName):

    hh=pysam.Samfile(ffm)
    REFs=hh.references
    
    N_all=0
    readLength=0

    readDict={}
    geneDict={}
    geneCoverage={}
    CHRlist=[]
    stList=[]
    edList=[]

    for gene in geneLoci:           
        tmp=gene.split(':')
        CHR=tmp[0]
        tmp=tmp[1].split('-')
        st=int(tmp[0])
        ed=int(tmp[1])
        if 'chr7' in REFs:
            CHRlist.append(CHR)
        else:
            CHRlist.append(re.sub('chr','',CHR))
        stList.append(st)
        edList.append(ed)
    
    ng=len(CHRlist)
    CHRlist=np.array(CHRlist)
    stList=np.array(stList)
    edList=np.array(edList)
    
    readsInGeneLoci=set()

    for read in hh.fetch(until_eof=True):
        if 'N' in read.seq: ## Remove reads with "N"
            continue
        if readLength==0:
            readLength=read.rlen
        if read.qname not in readDict:
            if re.search('[/][12]$', read.qname):
                ## explicit mate pair information
                if re.search('[/]1$', read.qname):
                    sq=re.sub('[/]1$','/2',read.qname)
                else:
                    sq=re.sub('[/]2$','/1',read.qname)
                if sq in readDict:
                    readDict[sq].append(read)
                else:
                    readDict[sq]=[read]
            elif re.search('[.][12]$', read.qname):
                ## explicit mate pair information
                if re.search('[.]1$', read.qname):
                    sq=re.sub('[.]1$','.2',read.qname)
                else:
                    sq=re.sub('[.]2$','.1',read.qname)
                if sq in readDict:
                    readDict[sq].append(read)
                else:
                    readDict[sq]=[read]                        
            else:
                readDict[read.qname]=[read]
        else:
                readDict[read.qname].append(read)
        
        CHR0=REFs[read.rname]
        pos=read.pos
        ssCHR=set(list(np.where(CHRlist==CHR0)[0]))
        ss_st=set(list(np.where(stList<=pos)[0]))
        ss_ed=set(list(np.where(edList>=pos)[0]))
        ss=list(ssCHR & ss_st & ss_ed)
        if len(ss)>0:
            readsInGeneLoci.add(read.qname)
            N_all+=1

            for s in ss:
                gg=geneName[s]+'|'+str(CHRlist[s])+':'+str(stList[s])+'-'+str(edList[s])
                if gg not in geneCoverage:
                    geneCoverage[gg]=[read]
                else:
                    geneCoverage[gg].append(read)     

                    
    
    PairedReadDict={}

    count0=0
    count1=0
    count2=0
    count3=0
    count4=0

    for kk in readDict:
        vv=readDict[kk]
        if len(vv)==1:
            continue
        if len(vv)>=2:
            ## multiple hits
            ## search soft-clipping reads          
            mflag=0
            for v in vv:
                if v.flag & 4 == 0:
                    score = my_mapping_quality_score(v.cigartuples, v.rlen)
                    if v.qname in readsInGeneLoci:
                        count0+=1
                        if score >= MAPQ_CUTOFF+0.05 and v.qname not in PairedReadDict:
                            count1+=1
                            mflag=1 
                            MappedRead = v
            
            if mflag == 0: #no mapped reads
                continue

            max_soft_clipping_score=0
            umflag=0 #no unmapped mate reads
            sflag=0

            for v in vv:

                if v.seq == MappedRead.seq:
                    continue

                if v.flag & 4 == 0 and 'S' not in v.cigarstring:
                    umflag=0
                    sflag=0
                    break

                if v.flag & 4 > 0:
                    MappedRead_mate = v
                    umflag=1
                    count2+=1
                    break
                else:
                    if 'S' in v.cigarstring:
                        count3+=1
                        sflag=1
                        
                        score = my_mapping_quality_score(v.cigartuples, v.rlen)
                        if max_soft_clipping_score < score:
                            tmp = v
                            max_soft_clipping_score = score


            if sflag == 1 and max_soft_clipping_score < MAPQ_CUTOFF+0.07:
                MappedRead_mate = tmp
                MappedRead_mate.flag = MappedRead_mate.flag - 4
                umflag=1
                count4+=1
            
            if mflag==1 and umflag==1:        
                vv = (2, [MappedRead, MappedRead_mate])
                #print MappedRead_mate.qname
                PairedReadDict[MappedRead_mate.qname]=vv

    
    for kk in PairedReadDict:
        vv=PairedReadDict[kk]
        if vv[0]==1:
            CHR0=REFs[vv[1][1].rname]
            pos=vv[1][1].pos
        if vv[0]==2:
            CHR0=REFs[vv[1][0].rname]
            pos=vv[1][0].pos
        ssCHR=set(list(np.where(CHRlist==CHR0)[0]))
        ss_st=set(list(np.where(stList<=pos)[0]))
        ss_ed=set(list(np.where(edList>=pos)[0]))
        ss=list(ssCHR & ss_st & ss_ed)
        if len(ss)==0:
            continue
        for s in ss:
            gg=geneName[s]+'|'+str(CHRlist[s])+':'+str(stList[s])+'-'+str(edList[s])
            if gg not in geneDict:
                geneDict[gg]=[vv]
            else:
                geneDict[gg].append(vv)

    #N_all+=len(PairedReadDict)
    #print 'Informative reads:', count0, count1, count2, count3, count4
    return readDict, PairedReadDict, geneDict, N_all, geneCoverage, readLength

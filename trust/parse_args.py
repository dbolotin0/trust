'''
Parsing trust arguments
'''


from optparse import OptionParser

def CommandLineParser():

    parser=OptionParser()

    print '''
        ===================================================================
        Tcr Repertoire Utilities for Solid Tissue, or TRUST, is a toolbox
        for analyzing T and B cell receptors in solid tumors using unselected
        RNA-seq data. TRUST performs de novo assembly of informative unmapped
        reads to estimate the TCR transcripts. It also estimates the CDR3 
        sequences and the relative fractions of different T or B cell 
        clonotypes. TRUST is developed by Bo Li (bli@jimmy.harvard.edu), 
        with all rights reserved. 
        ===================================================================
        '''

    parser.add_option("-d","--directory",dest="Directory",help="Input bam directory",default="")
    parser.add_option("-f","--file",dest="file",default='',help="Input bam file: if given, overwite -d option")
    parser.add_option("-F","--fileList",dest="files",default='',help='Alternative input: a file containing the full path to all the files. If given, overwrite -d and -f option')
    parser.add_option("-e","--error",dest="Error",type="int",default=1,help="Maximum number of sequencing error per repeating unit tolerated. Default: 1")
    parser.add_option("-l","--overlaplength",dest="Length",type="int",default=10,help="Minimum length of overlap sequence for calling reads overlap. Default 10")
    parser.add_option('-c',"--coverage",dest="Coverage",default=False,action="store_true",help='Whether or not output gene coverage. Default False')
    parser.add_option('-s',"--Single",dest="SingleEnd",default=False,action="store_true",help="If set True, TRUST will always run in single end mode")
    parser.add_option('-H',"--HeavyChainOnly",dest="HeavyChain",default=True,action="store_false",help="To save time, in single-end mode, TRUST only search for beta and delta chains unless this flag is set.")
    parser.add_option('-I','--InsertThreshold',dest="InsertThreshold",default=10,type='int',help="For PE library, when two mates overlap, TRUST cannot properly detect CDR3s based on mapped mates. Set larger value to force TRUST to run in PE mode despite small library insert size. Default 10.")
    parser.add_option('-o','--OutputDirectory',dest="WD",default="",help="Directory to store intermediate files and final TRUST report. User must have write privilege. If omitted, the current directory will be applied.")
    parser.add_option('-B','--Bcell',dest="Bcell",default=False,action="store_true",help="B cell receptor inference is currently under active development.")
    parser.add_option('-P','--LocusFile',dest="Locus",default=False,action="store_true",help="Use intermediate file, skip read screen. Input *-Locs.bam files.")
    parser.add_option('-U','--UnmappedDisable',dest="UD",default=False,action="store_true",help="This option disable usage of unmapped reads to accelerate calling.")
    parser.add_option('-g','--genome',dest="Genome",default="hg19",help="Reference genome of input bam file. Default: hg19")
    parser.add_option('-n','--CoreN',dest="NumberofCores",default="1",help="Using the multiprocessing to accelerate. Default: 1")
    parser.add_option('-L','--LightChain',dest="light_chain",default=False,action="store_true",help="With -B, this option runs light chain. Default False")
    parser.add_option("-E","--extension",dest="Extension",default=False,action="store_true",help="this option runs partial cdr3 extension, currently only support TCR. Default False")

    return parser.parse_args()
 
   
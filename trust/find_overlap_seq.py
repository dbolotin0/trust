'''
Find the overlap info between two sequences.
'''


import math
import regex
import getOverlapInfo_C
from trust.process_seq import *



def CompareSuffixByBit_C(y,x,n=50,err=1):
    '''
    Find the overlap sequence between x and y, tolerate up to err mismatches, currently gap is not supported
    Input is ordered. assuming x is in front of y
    x and y are two-bit coded, i.e. tuples with two integers
    n is the read length
    C speedup
    '''
    if x[0]==y[0] and x[1]==y[1]:
        return (n,0)
    res=[[]]*2
    if(n<64): 
        for ss in xrange(0,2):
            res[ss] = getOverlapInfo_C.compare(x[ss],y[ss], n)
    else:
        for ss in xrange(0,2):
            a = x[ss]
            pre = []
            i = math.ceil(n/64.0)
            while i > 0:            
                pre.append(a%(2**64))
                a=a/(2**64)
                i=i-1

            b = y[ss]
            suf = []
            j = math.ceil(n/64.0)
            while j > 0:
                suf.append(b%(2**64))
                b=b/(2**64)
                j=j-1

            res[ss] = getOverlapInfo_C.compare_Lgreaterthan64(pre, n, suf, n, 0)

    if(res[0][0] == res[1][0] and res[0][0] != -1): #equal overlap length
        err = res[0][1]+res[1][1]#mismatch count: 0,1,2
        if(res[0][2] == res[1][2]): #same mismatch position, return err=0or1
            return(res[0][0], err/2)
        elif(err == 1): #only single mismatch
            return(res[0][0], 1)
    
    return (0,0)


def PairwiseReadComparison1(xRead, yRead, x, y, n=50, err=1):
    '''
    Compare a pair of reads for their overlap in 4 scenarios: x,y; y,x; xc,y; y,xc
    '''
    suffList=[]

    xc=ReverseComp(x,n=n)
    xcRead=ReverseCompSeq(xRead)
    maxErr=1

    match1 = regex.search('('+yRead[0:10]+')' +'{s<='+str(maxErr)+'}', xRead)
    if match1:
        suf=CompareSuffixByBit_C(x,y,n=n,err=err)
        suffList.append(suf)
    else:
        suffList.append((0,0))

    match2 = regex.search('('+xRead[0:10]+')'  +'{s<='+str(maxErr)+'}', yRead)
    if match2:
        suf=CompareSuffixByBit_C(y,x,n=n,err=err) 
        suffList.append(suf)
    else:
        suffList.append((0,0))

    match3 = regex.search('('+yRead[0:10]+')'  +'{s<='+str(maxErr)+'}', xcRead)
    if match3:
        suf=CompareSuffixByBit_C(xc,y,n=n,err=err)
        suffList.append(suf)
    else:
        suffList.append((0,0))

    match4 = regex.search('('+xcRead[0:10]+')'  +'{s<='+str(maxErr)+'}', yRead)
    if match4:
        suf=CompareSuffixByBit_C(y,xc,n=n,err=err)    
        suffList.append(suf)
    else:
        suffList.append((0,0))

    return suffList


def PairwiseReadComparison(xRead, yRead, x,y,n=50,err=1):
    '''
    Compare a pair of reads for their overlap in 4 scenarios: x,y; y,x; xc,y; y,xc
    '''
    suffList=[]
    xc=ReverseComp(x,n=n)
    suf=CompareSuffixByBit_C(x,y,n=n,err=err)
    suffList.append(suf)
    suf=CompareSuffixByBit_C(y,x,n=n,err=err) 
    suffList.append(suf)
    suf=CompareSuffixByBit_C(xc,y,n=n,err=err)
    suffList.append(suf)
    suf=CompareSuffixByBit_C(y,xc,n=n,err=err)    
    suffList.append(suf)
    return suffList         


def CompareSuffixByBitSeq_C(y,x,ny,nx,err=1):
    '''
    Find the overlap sequence between x and y, tolerate up to err mismatches, currently gap is not supported
    Input is ordered. assuming x is in front of y
    x and y are two-bit coded, i.e. tuples with two integers
    nx, ny are the read length
    C speedup
    '''
    if x[0]==y[0] and x[1]==y[1]:
        return (nx,0)
    res=[[0, 0, 0]]*2
    for ss in xrange(0,2):
        a = x[ss]
        pre = []
        i = math.ceil(nx/64.0)
        while i > 0:            
            pre.append(a%(2**64))
            a=a/(2**64)
            i=i-1

        b = y[ss]
        suf = []
        j = math.ceil(ny/64.0)
        while j > 0:
            suf.append(b%(2**64))
            b=b/(2**64)
            j=j-1
        res[ss] = getOverlapInfo_C.compare_Lgreaterthan64(pre, nx, suf, ny, 1)

    if(res[0][0] == res[1][0] and res[0][0] != -1): #equal overlap length
        err = res[0][1]+res[1][1]#mismatch count: 0,1,2
        if(res[0][2] == res[1][2]): #same mismatch position, return err=0or1
            return(res[0][0], err/2)
        elif(err == 1): #only single mismatch
            return(res[0][0], 1)
    return (0,0)


def CompareSuffixSeq(seq1,seq2,err=1,count=False):
    '''
    Compare a pair of sequences for their overlap in 4 scenarios: x,y; y,x; xc,y; y,xc
    '''
    seq1c=ReverseCompSeq(seq1)
    x=ConvertDNAtoBinary(seq1)
    y=ConvertDNAtoBinary(seq2)
    xc=ConvertDNAtoBinary(seq1c)
    n1=len(seq1)
    n2=len(seq2)
    suffList=[]
    maxErr=1
    if count:

        suf=CompareSuffixByBitSeq_C(x,y,n1,n2,err=err)
        suffList.append(suf)
        suf=CompareSuffixByBitSeq_C(y,x,n2,n1,err=err)    
        suffList.append(suf)
        suf=CompareSuffixByBitSeq_C(xc,y,n1,n2,err=err)
        suffList.append(suf)
        suf=CompareSuffixByBitSeq_C(y,xc,n2,n1,err=err)   
        suffList.append(suf)

    else:
    #'''
        match1 = regex.search('('+seq2[0:10]+')'+'{s<='+str(maxErr)+'}', seq1)
        if match1:
            suf=CompareSuffixByBitSeq_C(x,y,n1,n2,err=err)
            suffList.append(suf)
        else:
            suffList.append((0,0))

        match2 = regex.search('('+seq1[0:10]+')' +'{s<='+str(maxErr)+'}', seq2)
        if match2:
            suf=CompareSuffixByBitSeq_C(y,x,n1,n2,err=err) 
            suffList.append(suf)
        else:
            suffList.append((0,0))

        match3 = regex.search('('+seq2[0:10]+')' +'{s<='+str(maxErr)+'}', seq1c)
        if match3:
            suf=CompareSuffixByBitSeq_C(xc,y,n1,n2,err=err)
            suffList.append(suf)
        else:
            suffList.append((0,0))

        match4 = regex.search('('+seq1c[0:10]+')' +'{s<='+str(maxErr)+'}', seq2)
        if match4:
            suf=CompareSuffixByBitSeq_C(y,xc,n1,n2,err=err)    
            suffList.append(suf)
        else:
            suffList.append((0,0))
  
    MaxL=-1
    MaxOs=-1
    MaxErr=0
    for i in xrange(0,4):
        tmp=suffList[i]
        if tmp[0]>MaxL:
            MaxL=tmp[0]
            MaxOs=i
            MaxErr=tmp[1]
    #print seq1, seq2, (MaxL,MaxErr)
    return ((MaxL,MaxErr),MaxOs)

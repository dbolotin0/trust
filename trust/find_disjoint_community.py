'''
Find disjoint communities from sequence overlap matrix.
'''


def dfs(graph, start):
    '''
    Non-resursive depth first search
    '''
    visited = set()
    stack = [start]
    while stack:
        vertex = stack.pop()
        if vertex not in visited:
            visited.add(vertex)
            stack.extend(set(graph[vertex]) - visited)
    
    return visited

def FindDisjointCommunities(OverlapInfo):

    kk=OverlapInfo.keys()
    OpDict={}
    
    for k in kk:
        k=k.split('\t')
        if k[0] in OpDict:
            OpDict[k[0]].append(k[1])
        else:
            OpDict[k[0]]=[k[1]]
        if k[1] in OpDict:
            OpDict[k[1]].append(k[0])
        else:
            OpDict[k[1]]=[k[0]]
    
    uniqReads=OpDict.keys()
    DisComm = []
    tmpl = set()
    
    for i in uniqReads:
        if(i not in tmpl):
            visited = dfs(OpDict, i)
            tmpl.update(visited)
            DisComm.append(sorted(list(visited)))
    
    #print "FindDisjointCommunities:", len(DisComm)
    
    return DisComm,OpDict

#!/usr/bin/env python
'''
TRUST set up script
'''

import os, sys, re
from setuptools import setup, Extension
from distutils.sysconfig import get_python_inc

NAME = 'trust'
PACKAGE = [NAME]
VERSION = __import__(NAME).__version__
try:
    f = open("requirements.txt", "rb")
    REQUIRES = [i.strip() for i in f.read().decode("utf-8").split("\n")]
except:
    print("'requirements.txt' not found!")
    REQUIRES = list()

incdir = get_python_inc(plat_specific=1)


def path_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            if filename[0] is not '.':  # filter hidden files
                paths.append(os.path.join(re.sub(NAME+'/', '', path), filename))
    return paths

package_files = path_files(NAME+'/data')


def main():
    setup(name=NAME,
        version=VERSION,
        description='Tcr&Bcr Repertoire Utilities for Solid Tumors',
        long_description=open('README.txt').read(),
        author='Bo Li, Jian Zhang',
        author_email='bli@jimmy.harvard.edu, zhangjianzn@gmail.com',
        url='https://bitbucket.org/liulab/trust/',
        packages=PACKAGE,
        package_dir={NAME: NAME},
        package_data={NAME: package_files},
        ext_modules=[Extension('trust/getOverlapInfo_C', ['trust/getOverlapInfo.cpp'], include_dirs=[incdir])],
        scripts=['bin/trust'],
        install_requires=REQUIRES,
        license=open('LICENSE').read()
        )


if __name__ == '__main__':
    main()